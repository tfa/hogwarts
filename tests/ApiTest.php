<?php
require './vendor/autoload.php';
use classes\Crud as Crud;

class ApiTest extends PHPUnit\Framework\TestCase {
	
		private $client;
		
	
		/**
		* @test - The test returns character with house code
		*/
		public function testApiGetCharacterByHouse()
		{
			$characterApi = new Crud('./characters.json');
			$object = json_decode($characterApi->actionReadByParam('1760529f-6d51-4cb1-bcb1-25087fce5bde'));
			$this->assertObjectHasAttribute('house', $object);
		}
		
		
		/**
		* @test Returns all Characters in file
		*/
		public function testApiGetAllCharacters()
		{
			$characterApi = new Crud('./characters.json');
			$this->assertIsArray($characterApi->actionRead());
		}
		
		/**
		* @test Check if file exists in application root directory
		*/
		public function testApiFileExists()
		{
			$this->assertFileExists('./characters.json');
		}
		
		/**
		* @test Check if file is Readable
		*/
		public function testApiFileReadable()
		{
			$this->assertFileExists('./characters.json');
		}
		
		/**
		* @test Check if file is Writable
		*/
		public function testApiFileWritable()
		{
			$this->assertFileExists('./characters.json');
		}
		
		
		/**
		* @test Check delete json file element
		*/
		public function testApiDeleteCharacter()
		{
			$this->expectException(\Exception::class);
			$this->expectExceptionMessage('Dont have nothing to delete. This id does not exist');
			$characterApi = new Crud('./characters.json');
			$characterApi->actionDelete();
			/*GET EXCEPTION BECAUSE DONT HAVE ARGUMENTS OR DO NOT EXIST*/			
		}
		
		/**
		* @test GET API CONTENT
		*/
		public function testGet()
		{
			$this->client = new GuzzleHttp\Client(['http://localhost/Hogwarts']);
			
			/* API POST DATA BY PARAM*/
			$response = $this->client->get('/api/characters', [
				'query' => [
					'house' => '34783453u4h5343454353'
				]
			]);

			$this->assertEquals(200, $response->getStatusCode());

			$data = json_decode($response->getBody(), true);

			/*ASSET METHODS TO VERIFY*/
			$this->assertArrayHasKey('name', $data);
			$this->assertArrayHasKey('role', $data);
			$this->assertArrayHasKey('school', $data);
			$this->assertArrayHasKey('house', $data);
			$this->assertArrayHasKey('patronus', $data);

		}

		/**
		* @test POST API CONTENT
		*/
		public function testPOST()
		{
			/* API POST DATA */
			$this->client = new GuzzleHttp\Client(['http://localhost/Hogwarts']);
			$data = array(
				'name' => 'TEST API',
				'role' => 'employee',
				'school'=> 'SOMETHING',
				'house' => '23534895h34u5h34iu534593425345kj',
				'patronous' => 'spider'
			);

			/*CALL POST METHOD*/
			$request = $this->client->post('/api/characters',$data);
			$response = $request->send();
			
			/*ASSET METHODS TO VERIFY*/
			$this->assertContains('23534895h34u5h34iu534593425345kj', $response);
			 $this->assertEquals(201, $response->getStatusCode());
			$this->assertTrue($response->hasHeader('Location'));
			$data = json_decode($response->getBody(true), true);
			$this->assertArrayHasKey('house', $data);
		}
}
?>