<?php
namespace classes;
require '../vendor/autoload.php';

use Phpfastcache\CacheManager;
use Phpfastcache\Drivers\Files\Config as FilesConfig;


class Crud
{
	/*ATTRIBUTES*/
    private $homePath;
    private $filePath;
    private $fileContent;
	private $ckey;
    public $data;
    public $attributesList;
	public $character;

	/*Class Constructor*/
    public function __construct($filePath)
    {
        $this->homePath = $_SERVER['PHP_SELF']; //HOME PATH
		
		/*CREATE CRUD INSTANCE AND PUT JSON DATA INTO CLASS ATTIBUTE IF HAS HOME PATH*/
        if (file_exists($filePath)) {
            $this->filePath = $filePath;
           
			$this->attributesList = ["name", "role", "school" , "house", "patronus"];
			
			/*CACHE Config*/
			$config = new FilesConfig();
			$config->setPath('./');
			$config->setSecureFileManipulation(true);
			
			/*CACHE INSTANCE - Dir cache*/
			$cacheInstance = CacheManager::getInstance('Files', new FilesConfig([
			  'path' => './cache']
			));

			/*CACHE KEY*/
			$this->ckey = "house";
			
			/*CACHE STRING - GET the element*/
			$cachedString = $cacheInstance->getItem($this->ckey);
			
			/*IF ELEMENT FOUNDED*/
			if (!$cachedString->isHit()) {
				
				$this->fileContent = file_get_contents($filePath);
				$this->data = json_decode($this->fileContent, true);
			
				/*SAVE INTO CACHE*/
				$cachedString->set($this->data)->expiresAfter(300);
				$cacheInstance->save($cachedString);
				
				/*MESSAGE NO CACHE*/
				 echo " ************ NO CACHE ************  ---> API RUN FIRST TIME <--- ";
				 //var_dump( $cachedString->get());
			}else{
				$this->data = $cachedString->get();
				//var_dump($cachedString->get());
				echo " ************ USE CACHE ************  ";
			}
			
			 return $cachedString->get();
			  $cacheInstance->clear();
			 
        } else {
            throw new \Exception("No file found", 1);
        }
    }

	/*Method To ADD OR EDIT DATA ( POST )*/
    public function actionAddEdit()
    {	
		/*GET POST DATA ID*/
        if (isset($_POST["id"])) {
			
			/****EDIT****/
			
            $id = $_POST["id"];
			
			/*GET DATA WITH POST ID*/
            $data = $this->data;			
            $itemData = $data[$id];
			
			/*LOOP JSON DATA ATTRIBUTES AND MAKE AN ARRAY WITH NEW VALUES*/
            foreach ($this->attributesList as $value) {
                $post[$value] = isset($_POST[$value]) ? $_POST[$value] : "";
            }
			
			/*UPDATE JSON DATA VALUES THAT HAS THE SAME POST ID*/
            if ($itemData) {
                unset($data[$id]);
                $data[$id] = $post;
                file_put_contents($this->filePath, json_encode($data));
				echo "EDITED";
            }
		}else{
			
			/****ADD****/
			
			/*ALL JSON DATA*/
			$data = $this->data;
			$data = ($this->data == null) ? array() : $this->data;
			
			/*INSERT NEW JSON DATA VALUES INTO ARRAY*/
			array_push($data, array('name'=>$_POST['name'] , 'role'=> $_POST['role'],  'school'=> $_POST['school'] ,  'house'=> $_POST['house'], 'patronus'=> $_POST['patronus']));
			
			/*Put ARRAY DATA INTO JSON FILE*/
			file_put_contents($this->filePath, json_encode($data));
			echo "ADDED";
			
		}
		
		echo json_encode($this->data);
		return $this->data; //RETURN DATA
    }

	/*Method to read DATA ( GET )*/
    public function actionRead()
    {
		echo json_encode($this->data);
		/*Returns ALL JSON DATA*/
        return $this->data; //RETURN DATA
    }
	
	/*Method to read DATA BY PARAM ( GET )*/
	 public function actionReadByParam($param)
    {
		/*ALL JSON DATA*/
		$contents = $this->data;
			 
		/*ALL JSON DATA LOOP*/
		foreach($contents as $content)
		{
			//GET ELLEMENT BY PARAM (HOUSE CODE)
			if($content['house'] == $param){
				$this->character = $content;
			}
			
		}
		echo json_encode($this->character);
		return json_encode($this->character); //RETURN DATA
    }

	/*Method to delete DATA ( GET )*/
    public function actionDelete($id=null)
    {
		/*HAS ID*/
        if ($id!==null && is_numeric($id) && $this->data[$id]) {
			/*GET ALL JSON DATA*/
            $data = $this->data;
			
			/*REMOVE DATA THAT HAS ID FROM JSON DATA ARRAY*/
            unset($data[$id]);
			
			/*SEND TO JSON THE ARRAY WITHOUT REMOVED DATA*/
            file_put_contents($this->filePath, json_encode($data));
			
			echo json_encode($this->data);
            return $this->data;//RETURN DATA
        } else {
            throw new \Exception("Nothing to delete");
        }
    }
}
