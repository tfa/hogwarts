<?php 
namespace classes;

include_once('../classes/Crud.php'); // CRUD CLASS
$crud = new Crud('../characters.json'); //JSON FILE

/*GET THE REQUEST METHOD*/
$method = $_SERVER['REQUEST_METHOD'];

switch ($method) {
	case 'PUT':
	case 'POST':
		/*ADD OR EDIT JSON DATA*/
		$crud->actionAddEdit();  
		break;
	case 'GET':
		 if (isset($_GET["house"])) {
			 /*BRING ONLY DATA BY HOUSE*/
			$crud->actionReadByParam($_GET["house"]);
		}else{
			/*BRING ALL JSON DATA*/
			$crud->actionRead();
		}
		break;
	case 'DELETE':
		/*DELETE JSON DATA*/
		$crud->actionDelete($_REQUEST["id"]);
		break;
   default:
		/*ERROR*/
		handle_error($request);  
		break;
}
?>